import numpy as np
from numpy import random
import matplotlib.pyplot as plt
import pdb

#parameters

#propability a deer appears
deerProb = 0.001

#maximum velocity a car can go
speedLimit = 1. #mpm

#distance to see a deer
deer_sight_distance = 1.5

#how much cars decelerate when they see a deer
decelerate = 0.2

#maximum possible acceleration rate
a_max = 0.1

#closest cars can possibly get
dx_max = 0.1


#Defining Car class to have position and velocity
class Car(object):
    def __init__(self, position, velocity, license_num):
        self.position = position
        self.velocity = velocity
        self.license_num = license_num
    def move(self): #Moves the car with its current velocity
        self.position = (self.position + self.velocity)
    def change_velocity(self, new_velocity): #Change the car's velocity
        self.velocity = new_velocity

#Defining deer function
def deer():
    prob = np.random.random()
    if prob < deerProb:
        return True
    else:
        return False

#Defining velocity_check where it takes two cars and returns a new velocity
def vel_check(car1, car2):
    dx = car1.position - car2.position
    dvel = car1.velocity - car2.velocity
    velocity = car2.velocity
    velocity1 = car1.velocity
    if dvel==0: #velocities of the cars are the same
        if dx>1.1*velocity: #distance between cars exceeds 1.1
            vel_new=velocity*1.1 #car speeds up 
        elif dx<0.9*velocity: #distance between cars 
            vel_new=velocity*0.9 #car slows down
        else:
            vel_new=dx*velocity# (velocity-(1.0-dx))   
    #first car faster than second car
    elif dvel>0:
        dt=dx/dvel #time it would take for car to catch up
        #if they are closer than 1., second car does not accelerate
        if dx < dx_max*2.:
            vel_new = velocity
        #if they are farther than 1., second car accelerates
        else:
            vel_new = (velocity1 + velocity) / 2.
            if vel_new - velocity > a_max:
                vel_new = velocity + a_max
    #second car is faster than first car
    elif dvel<0: 
        dt=-dx/dvel #time it would take for the cars to crash
        #if they will crash NOW - keep them min distance apart
        if dt<1.0:
            vel_new = dx - dx_max
        #if they will crash next time step - slow second car
        elif dt<=2.0:
            vel_new=velocity-(dx_max/2) #more gradual slow down if they would crash in the next 2 time steps
        elif dt<=3.0:
            vel_new=velocity-(dx_max/3) #more gradual slow down if they would crash in the next 3 time steps
        elif dx>1.1*velocity: #they are a same distance
            vel_new=1.1*velocity
        elif dx<0.9*velocity:
            vel_new=0.9*velocity
        elif dx<=1.1*velocity and dx>=0.9*velocity:
            vel_new=dx*velocity# (velocity-(1.0-dx))
            
    if vel_new >= speedLimit:
        vel_new = speedLimit
    if vel_new <= 0:
        vel_new = 0
    return vel_new




#Defining the simulation with number of cars and speed limit
def highway(num_cars, max_vel, max_pos):
    num_exit = 0 #Number of cars past highway
    num_entered = 0 #Total number of cars that entered the simulation
    dt = 1.0 #Time step length
    time = 0. - dt #Current time. Negative so we can start at time = 0
    car_list = [] #Define array of cars
    deer_count = 0
    history = []
    deer_history = []

    skip = 0
    
    while num_exit < num_cars:
        time += dt #Advance time
        remove_list = [] #List of cars that can be removed after they leave highway
        deer_positions = [] #make array of mile markers where there are deer at this time step

        for i in range(len(car_list)):
            hist_car = car_list[i] #to keep track of where each car has been
            history.append([hist_car.license_num, time, hist_car.position, hist_car.velocity])
        
        for i in range(101):
            if deer() == True:
                deer_positions.append(float(i))
                deer_count += 1
            
        for i in range(len(car_list)): #Changes velocity for all cars, deer included
            for j in range(len(deer_positions)):
                if float(deer_positions[j]) - car_list[i].position < deer_sight_distance and car_list[i].position <= float(deer_positions[j]):
                    car_list[i].change_velocity(car_list[i].velocity * decelerate)
                
            if i == 0:
                if car_list[i].velocity < speedLimit:
                    car_list[i].change_velocity(car_list[i].velocity + a_max)
                    if car_list[i].velocity > speedLimit:
                        car_list[i].change_velocity(max_vel) 
            else:
                vel_n = vel_check(car_list[i-1],car_list[i])
                car_list[i].change_velocity(vel_n)
        
        for i in range(len(car_list)): #Moves all the cars
            car_list[i].move()
            if car_list[i].position > max_pos: #Finds cars past highway
                remove_list.append(i)

        for n in remove_list[::-1]: #Removes cars
            car_list.pop(n)
            num_exit += 1



        #allow the first car to enter the highway
        if time == 0:
                car = Car(0.0, max_vel, num_entered)
                car_list.append(car)
                num_entered += 1
                
        #now that there's at least one car on the highway...        
        if time != 0 and num_entered < num_cars:
            #if there's a car at position 0 that's slowed down, count up
            mostrecentcar_v = car_list[len(car_list) -1].velocity
            mostrecentcar_x = car_list[len(car_list) -1].position
            if mostrecentcar_v < speedLimit and mostrecentcar_x == 0.0:
                skip += 1

            #otherwise... add another car
            else:
                car = Car(0.0, max_vel, num_entered)
                car_list.append(car)
                num_entered += 1
        

        car_positions = []
        for i in range(len(car_list)):
            x = car_list[i].position
            car_positions.append(x)
        
        #make array of an arbitrary y value of length car_list
        y = []
        for i in range(len(car_list)):
            height = 1.0
            y.append(height)

        y_deer = []
        for i in range(len(deer_positions)):
            height = 1.0
            y_deer.append(height)

        for pos in deer_positions:
            deer_history.append([time, pos])
        #plot positions for every time step
        #plot deer in red
        """
        if time < 30:
            plt.plot(car_positions, y, 'bo', deer_positions, y_deer, 'ro')
            plt.axis([0, 20, 0, 2])
            plt.show()
            print deer_count, num_exit
        """
    history.sort()
    return time, history, deer_history, deer_count
   

#plot the positions of cars over time
num = 1000
HW = highway(num, 1.0, 100.0)
print HW[0]


plt.plot([0,0], [0, HW[0]])
plt.xlabel('Time')
plt.ylabel('Position')
for i in range(num):
    time = []
    pos = []
    for hist in HW[1]:
        if hist[0] == i:
            time.append(hist[1])
            pos.append(hist[2])
    plt.plot(time, pos, 'b')
    plt.ylim(0, 20.0)
    plt.xlim(0, 200)
for dh in HW[2]:
    plt.plot(dh[0], dh[1], 'ro')
plt.grid()
plt.show() 